#include "userprog/syscall.h"
#include <stdio.h>
#include <stdbool.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "threads/synch.h"
#include "filesys/filesys.h"
#include "filesys/file.h"
#include "devices/input.h"
#include "lib/kernel/console.h"
#include "lib/string.h"
#include "lib/kernel/list.h"

// lock used by syscalls involving file system to ensure only one thread at a time 
// is accessing file system 
struct lock filesystem_lock; 
typedef int pid_t; // variable for integer

struct file_desc
{
  // the unique file descriptor number returns to user process.
  int open_file_num;
  // the owner thread’s thread id of the open file 
  pid_t owner;
  //file that is opened 
  struct file *file_struct;
  struct list_elem e;
};

static void syscall_handler (struct intr_frame *);

static void check_user (const uint8_t *uaddr);

// system calls implemented and written below
bool create (const char *file, unsigned size); //system call 1
pid_t write (int fd, const void *buffer, unsigned size); //system call 2
int exec (const char *cmd_line); //system call 3
int read (int fd, const void *buffer, unsigned size); //system call 4
bool remove (const char *remove_file); //system call 5

// system calls not implemented
void halt (void);
void exit (int status);
int wait (pid_t pid);
int open (const char *file);
int filesize (int fd);
void seek (int fd, unsigned position);
unsigned tell (int fd);
void close (int fd);

/**
 * Checks for the validity of a user address and should only be 
 * below the physical base as well as in the page directory
*/
bool valid (void *vaddress){
  return (is_user_vaddr(vaddress) && pagedir_get_page(thread_current() -> pagedir, vaddress) != NULL);
}

/**
 * @brief 
 * 
 */
void syscall_init (void) 
{
  lock_init(&filesystem_lock);
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

/**
 * @brief This will be used to perform each system call
 * 
 * @param f 
 */
static void syscall_handler (struct intr_frame *f) 
{
  int *c = f->esp; 

  if (!valid ((void *)c))
    exit(-1);

  int syscall_number = *c;

  switch (syscall_number){

    case SYS_CREATE:
      if (!valid(c+4) || !valid(c+5) || !(valid(*(c+4))))
        exit(-1);
      f-> eax = create((const char *)(c+4), *(c+5));
      break;

    case SYS_WRITE:
      if (!valid(c+5) || !valid(c+6) || !valid (c+7)|| !valid(*(c+6)))
        exit(-1);
      f-> eax = write(*(c+5),(void *)(c+6),*(c+7));
      break;
    
    case SYS_EXEC:
      if(!valid(c+1) || !valid(*(c+1)))
        exit(-1);
      f-> eax = exec((const char *)(c+1));
      break;

    case SYS_READ:
      if (!valid(c+5) || !valid (c+6) || !valid (c+7) || !valid (*(c+6)))
        exit(-1);
      f-> eax = read(*(c+5),(const void  *)(c+6),*(c+7));
      break;

    case SYS_REMOVE:
      if (!valid(c+1) || !valid(*(c+1)))
        exit(-1);
      f -> eax = remove((const char *)(c+1));

    default:
      // only if invalid system call number is entered
      printf("Invalid System call number\n");
      exit(-1);
      break;
  }
}

 /**
  * @brief Creates a new file with given initial size bytes. 
  * 
  * @param file the file which is to be created
  * @param size the size of the bytes
  * @return true file creation is successful
  * @return false returns false if fails where file already exists or if internal memory 
  * allocation fails.
  */
bool create(const char *file, unsigned size){
  bool file_status;
  int a = -1;

  if (file==NULL)
    return a;
  lock_acquire(&filesystem_lock); // lock set and sleeps until it becomes available
  file_status = filesys_create(file, size); // from filesys/filesys.h library

  lock_release(&filesystem_lock);
  return file_status;
}

/**
 * @brief Writes size bytes from buffer to the open file fd.
 * 
 * @param fd the file descriptor
 * @param buffer buffer to write the data to
 * @param size the number of bytes to write
 * @return pid_t the number of bytes that was written, which may be 
  less than size if some bytes could not be written
 */
pid_t write (int fd, const void *buffer, unsigned size)
{

  if (fd == STDOUT_FILENO){
    putbuf(buffer, size);
    return size;
  }

  // For fd other than 1, retrieve file_desc element structure variable
  struct  file_desc *fd_struct = get_file(fd);

  if (fd_struct == NULL){
    return -1;
  }
  // lock set and sleeps until it becomes available
  lock_acquire(&filesystem_lock); 
 
  // Retrieving length/size using filesys function for equivalent file pointer 
  //int res = file_write(fd_struct -> fd, buffer, size);

  lock_release(&filesystem_lock);
  return size;
}

/**
 * @brief Runs the executable whose name is given in cmd_line
 * 
 * @param cmd_line any given arguments that is passed 
 * @return pid_t returns the new process's program id (pid) otherwise returns -1 if pid 
 * is not valid
 */
pid_t exec (const char * cmd_line){

  lock_acquire(&filesystem_lock);
  char *saveptr;
  char passed_val = strtok_r(cmd_line, " ", &saveptr);
  int tid = process_execute(passed_val);
  lock_release(&filesystem_lock);
  return tid;
}

/**
 * @brief Reads size bytes from the file open as fd into buffer
 * 
 * @param fd the file descriptor
 * @param buffer buffer to read the data from
 * @param size the number of bytes/ the length of the buffer
 * @return int Returns the number of bytes actually read (0 at end of file), or -1 
 * if the file could not be read (due to a condition other than end of file)
 */
int read (int fd, const void *buffer, unsigned size){
  int l = 0;

  // If fd is equal to zero, it reads from keyboard using input_getc() from devices/input.c
  if (fd == STDIN_FILENO){
    while(l < size){
      *((char *) buffer+l) = input_getc();
      l++;
    }
    return l;
  }
  // checks to see if fd is anything other than zero, we can retrieve file_desc element
  struct file_desc *fd_elem = get_file(fd);
  if (fd_elem == NULL)
    return -1;
  
  lock_acquire(&filesystem_lock);
  //l = file_read(fd_elem -> fd, buffer, size);
  return l; // returns the number of bytes that are read
}

/**
 * @brief Deletes the file called file. Returns true if successful, false otherwise.  
 * 
 * @param remove_file the file that we are attempting to remove
 * @return true removing the file is succesful
 * @return false removing the file has failed
 */
bool remove (const char *remove_file){
  if (remove_file == NULL){
    return -1;
  }
  lock_acquire(&filesystem_lock);
  bool confirm = filesys_remove(remove_file);
  lock_release(&filesystem_lock);

  return confirm;
}